#include <stdio.h>
#include <assert.h>

#include "mem.h"
#include "mem_internals.h"

static struct block_header * get_header(const void * contents) {
    return (struct block_header *) ((uint8_t *) contents - offsetof(struct block_header, contents));
}

#define ALLOC_AND_GET_INFO(postfix, size)                                              \
const size_t block_size ## postfix = size;                                             \
void * block_contents ## postfix = _malloc(block_size ## postfix);                               \
struct block_header * block_header ## postfix = get_header(block_contents ## postfix); \


int main() {
    const size_t heap_size = 1000;
    void * heap = heap_init(heap_size);
    struct block_header * heap_start = heap;
    assert(heap_start);

    puts("Single alloc");
    ALLOC_AND_GET_INFO(_1, 100);
    assert(!block_header_1->is_free);
    assert(block_header_1->capacity.bytes == block_size_1);
    debug_heap(stdout, heap);
    puts("Single alloc: success\n");

    puts("Continuous alloc");
    ALLOC_AND_GET_INFO(_2, 200);
    assert(!block_header_2->is_free);
    assert(block_header_2->capacity.bytes == block_size_2);
    ALLOC_AND_GET_INFO(_3, 300);
    assert(!block_header_3->is_free);
    assert(block_header_3->capacity.bytes == block_size_3);
    debug_heap(stdout, heap);
    puts("Continuous alloc: success\n");

    puts("Grow heap alloc");
    ALLOC_AND_GET_INFO(_extend, 2000);
    assert(!block_header_extend->is_free);
    assert(block_header_extend->capacity.bytes == block_size_extend);
    debug_heap(stdout, heap);
    puts("Grow heap alloc: success\n");

    puts("Free middle block");
    _free(block_contents_2);
    assert(block_header_2->is_free);
    debug_heap(stdout, heap);
    puts("Free middle block: success\n");

    puts("Free start block, next is free");
    _free(block_contents_1);
    assert(block_header_1->is_free);
    assert(block_header_1->next == block_header_3);
    debug_heap(stdout, heap);
    puts("Free start block, next is free: success\n");

    return 0;
}
