#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header * b, const char * fmt, ...);

void debug(const char * fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, const struct block_header * block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void * restrict addr, block_size block_sz, void * restrict next) {
    *((struct block_header *) addr) = (struct block_header) {.next = next, .capacity = capacity_from_size(
            block_sz), .is_free = true};
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);

static void * map_pages(void const * addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const * addr, size_t query) {
    size_t size_to_alloc = region_actual_size(query);
    void * map_result = map_pages(addr, size_to_alloc, MAP_FIXED_NOREPLACE);
    bool extends = true;
    if (map_result == MAP_FAILED) {
        map_result = map_pages(addr, size_to_alloc, 0);
        extends = false;
    }
    struct region region = { map_result,size_to_alloc, extends};
    block_init(map_result, (block_size) {size_to_alloc}, NULL);
    return region;
}

static void * block_after(const struct block_header * block);

void * heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(const struct block_header * restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header * block, size_t query) {
    if (!block_splittable(block, query)) {
        return false;
    }
    const block_capacity initial_capacity = block->capacity;
    block->capacity = (block_capacity) {query};
    void * split_right = block_after(block);
    block_init(split_right, (block_size) {initial_capacity.bytes - query}, NULL);
    block->next = split_right;
    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void * block_after(const struct block_header * block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(const struct block_header * fst, const struct block_header * snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(const struct block_header * restrict fst, const struct block_header * restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header * block) {
    const struct block_header * block_right = block->next;
    if (!block_right || !mergeable(block, block_right)) {
        return false;
    }
    block->capacity.bytes += size_from_capacity(block_right->capacity).bytes;
    block->next = block_right->next;
    return true;
}

/*  --- ... если размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header * block;
};

static struct block_search_result find_good_or_last(struct block_header * restrict block, size_t sz) {
    struct block_header * cur_block = block;
    while (true) {
        if (cur_block->is_free && block_is_big_enough(sz, cur_block)) {
            return (struct block_search_result) { BSR_FOUND_GOOD_BLOCK, cur_block };
        }
        if (!cur_block->next) {
            break;
        }
        cur_block = cur_block->next;
    }
    return (struct block_search_result) { BSR_REACHED_END_NOT_FOUND, cur_block };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header * block) {
    const struct block_search_result bsr = find_good_or_last(block, query);
    if (bsr.type == BSR_REACHED_END_NOT_FOUND) {
        return bsr;
    }
    split_if_too_big(bsr.block, query);
    return bsr;
}

static struct block_header * grow_heap(struct block_header * restrict last, size_t query) {
    const void * extend_addr = block_after(last);
    const struct region new_region = alloc_region(extend_addr, query);
    if (region_is_invalid(&new_region)) {
        return NULL;
    }
    last->next = new_region.addr;
    return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header * memalloc(size_t query, struct block_header * heap_start) {
    size_t actual_query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result bsr = try_memalloc_existing(actual_query, heap_start);
    if (bsr.type == BSR_FOUND_GOOD_BLOCK) {
        bsr.block->is_free = false;
        return bsr.block;
    }
    if (bsr.type == BSR_REACHED_END_NOT_FOUND && grow_heap(bsr.block, actual_query)) {
        try_merge_with_next(bsr.block);
        bsr = try_memalloc_existing(actual_query, heap_start);
        bsr.block->is_free = false;
        return bsr.block;
    }
    return NULL;
}

void * _malloc(size_t query) {
    struct block_header * const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header * block_get_header(void * contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void * mem) {
    if (!mem) return;
    struct block_header * header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
